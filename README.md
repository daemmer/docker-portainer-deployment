Portainer Docker-Compose Deployment
---

Portainer is a simple management UI for Docker.
https://portainer.io/

This docker-compose deployment is a very simple example. For more and deeper deployment examples: https://portainer.readthedocs.io

## Deployment Steps

1. create and customise your environment-file(`.env`-file)
2. `docker-compose up -d`

## Dependencies

My private deployments depends on a external network called `hucnet_proxy``

```bash
networks:
  hucnet_proxy:
    external: true
```
> For your custom network replace all occurrences of `hucnet_proxy` and remove `external: true` if not.

## Configuration

### traefik labels

Traefik's Labels are optional if you not prefer [traefik](https://traefik.io/) as reverse-proxy.

```yml
labels:
  - "traefik.enable=true"
  - "traefik.domain=${PORTAINER_FQDN}"
  - "traefik.frontend.entryPoints=http,https"
  - "traefik.frontend.redirect.entryPoint=https"
  - "traefik.frontend.rule=Host: ${PORTAINER_FQDN}"
  - "traefik.port=9000"
  - "traefik.frontend.whiteList.useXForwardedFor=true"
  - "traefik.backend=portainer"
```

## Your custom environment variables for Compose (`.env`)

```bash
#!/usr/bin/env
# environment variables for Docker-Compose
# Source: https://docs.docker.com/compose/environment-variables/#the-env-file
#

PORTAINER_VERSION=latest

PORTAINER_FQDN=<yourdomain>

# Quad9 and Google
DNS1=9.9.9.9
DNS2=8.8.8.8
```
